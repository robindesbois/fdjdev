//
//  Team.swift
//  FDJDev
//
//  Created by Regis Alla on 17/09/2022.
//

import Foundation

struct Team: Codable {
    var idTeam: String?
    var country: String?
    var league: String?
    var description: String?
    var teamBanner: String?
    var name : String?
    var teamBadge : String?
    
    private enum CodingKeys: String, CodingKey {
        case idTeam = "ref"
        case country = "strCountry"
        case league = "strLeague"
        case description = "strDescriptionFR"
        case teamBanner = "strTeamBanner"
        case name = "strTeam"
        case teamBadge = "strTeamBadge"
    }
}
