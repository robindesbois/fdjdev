//
//  CompetitionCell.swift
//  FDJDev
//
//  Created by Regis Alla on 17/09/2022.
//

import Foundation
import UIKit

protocol ReusableView: AnyObject {
    static var identifier: String { get }
}

class TeamCell: UICollectionViewCell {
    static let cellId = "CellTeam"
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    func setup(with image: UIImage) {
        logoImageView.image = image
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
        self.setupLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureImage(urlString: String?) {
        if let url = urlString, let image = ImageCache.shared.cache.object(forKey: url as NSString) {
            logoImageView.image = image
        }
    }
    
    private func setupViews() {
        backgroundColor = UIColor.white
        addSubview(logoImageView)
    }
    
    private func setupLayouts() {
        self.logoImageView.translatesAutoresizingMaskIntoConstraints = false
        
        // Layout constraints for `TeamCell`
        self.logoImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 16).isActive = true
        self.logoImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16).isActive = true
        self.logoImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16).isActive = true
        self.logoImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16).isActive = true
    }
}

extension TeamCell: ReusableView {
    static var identifier: String {
        return String(describing: self)
    }
}
