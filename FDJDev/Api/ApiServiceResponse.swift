//
//  ApiServiceResponse.swift
//  FDJDev
//
//  Created by Regis Alla on 17/09/2022.
//

import Foundation
import Alamofire

public enum RequestResult<T>{
    case done(T)
    case failed(message: String)
}

let SERVER_NOT_FOUND = 502
let SERVER_NOT_RESPONDING = 504
let ERROR_SERVER = 500

class ErrorHandler {
    
    enum ErrorType: LocalizedError {
        case networkError
        case serverNotResponding
        case parsingError
        case serverNotFound
        case errorServer
        case unknownError
        
        var errorDescription: String {
            switch self {
            case .networkError:
                return NSLocalizedString("ERR0R_NETWORK", comment: "")
            case .serverNotResponding:
                return NSLocalizedString("SERVER_NOT_RESPONDING", comment: "")
            case .parsingError:
                return NSLocalizedString("ERR0R_PARSING", comment: "")
            case .serverNotFound:
                return NSLocalizedString("SERVER_NOT_FOUND", comment: "")
            case .errorServer:
                return NSLocalizedString("ERR0R_SERVER", comment: "")
            case .unknownError:
                return NSLocalizedString("ERROR_UNKNOW", comment: "")
            }
        }
    }
    
    static func getErrorType(forServerError error : Error)-> ErrorType {
        guard NetworkReachabilityManager()!.isReachable else{
            return ErrorType.networkError
        }
        
        if let serverError =  error as? AFError {
            if let  code = serverError.responseCode {
                return getErrorType(fromStatusCode: code)
            }
        }
        return ErrorType.serverNotResponding
    }
    
    static func getErrorType(fromStatusCode code : Int)-> ErrorType {
        switch code {
        case SERVER_NOT_FOUND:
            return ErrorType.serverNotFound
        case SERVER_NOT_RESPONDING:
            return ErrorType.serverNotResponding
        case ERROR_SERVER:
            return ErrorType.errorServer
        default:
            return ErrorType.unknownError
        }
    }
}
