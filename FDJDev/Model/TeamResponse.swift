//
//  TeamResponse.swift
//  FDJDev
//
//  Created by Regis Alla on 17/09/2022.
//

import Foundation

struct TeamResponse: Codable {
    var teams: [Team]?
}
