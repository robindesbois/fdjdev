//
//  MockApiService.swift
//  FDJDevTests
//
//  Created by Regis Alla on 19/09/2022.
//

import Foundation
@testable import FDJDev

final class MockApiService: ApiServiceProtocol {
    var resultLeague: RequestResult<[League]?> = .done([])
    var resultSearchTeam: RequestResult<[Team]?> = .done([])
    
    func getAllLeagues(completion: @escaping (RequestResult<[League]?>) -> Void) {
        completion(resultLeague)
    }
    
    func searchTeamByQuerry(text: String, completion: @escaping (RequestResult<[Team]?>) -> Void) {
        completion(resultSearchTeam)
    }
    
    func downloadImageWithPath(imagePath: String, completionHandler: @escaping (Bool) -> ()) {
        
    }
}
