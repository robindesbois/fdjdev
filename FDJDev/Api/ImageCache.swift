//
//  ImageCache.swift
//  FDJDev
//
//  Created by Regis Alla on 19/09/2022.
//

import Foundation
import UIKit

class ImageCache {
    static let shared = ImageCache()
    var cache: NSCache<NSString, UIImage> = NSCache()

    
    private init() {
         cache = NSCache()
    }
}
