//
//  CompetitionListViewController.swift
//  FDJDev
//
//  Created by Regis Alla on 17/09/2022.
//

import Foundation
import UIKit

private enum LayoutConstant {
    static let spacing: CGFloat = 16.0
    static let padding: CGFloat = 50
}

protocol TeamListViewProtocol {
    func reloadList()
    func dissmissSuggestionTableView()
    func showSuggestionTableView()
    func displayLoading()
    func dismissLoading()
    func showFailed(message: String)
    func updateList(indexPath: IndexPath)
    func showDetails(detailPresenter: TeamDetailPresenter)
}

class TeamListView: UIViewController {
    
    lazy private var presenter : TeamListViewPresenter = TeamListViewPresenter()
    
    private lazy var loading: UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView(style: .medium)
        loading.translatesAutoresizingMaskIntoConstraints = false
        loading.hidesWhenStopped = true
        return loading
    }()
    
    private lazy var collectionView: UICollectionView = {
        let viewLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: viewLayout)
        collectionView.backgroundColor = .white
        return collectionView
    }()
    
    private lazy var searchBar: UISearchController = {
        let searchBar = UISearchController()
        searchBar.searchBar.placeholder = NSLocalizedString("ENTER_LEAGUE_NAME", comment: "")
        searchBar.searchBar.searchBarStyle = .minimal
        return searchBar
    }()
    
    private lazy var completionTableView: UITableView = {
        let completionTableView = UITableView()
        completionTableView.backgroundColor = .white
        completionTableView.translatesAutoresizingMaskIntoConstraints = false
        return completionTableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupLayouts()
        
        presenter.setView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.fetchAllLeagues()
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        navigationItem.title  = NSLocalizedString("NAVIGATION_BAR_TITLE", comment: "")
        searchBar.searchResultsUpdater = self
        searchBar.resignFirstResponder()
        
        navigationItem.searchController = searchBar
        view.addSubview(collectionView)
        view.addSubview(loading)
        view.addSubview(completionTableView)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(TeamCell.self, forCellWithReuseIdentifier: TeamCell.identifier)
        
        completionTableView.delegate = self
        completionTableView.dataSource = self
        completionTableView.isScrollEnabled = true
        completionTableView.isHidden = true
    }
    
    private func setupLayouts() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        // Layout constraints for `collectionView`
        self.collectionView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        self.loading.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.loading.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
        
        self.completionTableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        self.completionTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.completionTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: LayoutConstant.spacing).isActive = true
        self.completionTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -LayoutConstant.spacing).isActive = true
    }
}

//MARK: - Collection data source and delegate
extension TeamListView:  UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.teamListCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let teamCell = collectionView.dequeueReusableCell(withReuseIdentifier: TeamCell.identifier, for: indexPath) as! TeamCell
        
        teamCell.configureImage(urlString: presenter.downloadImageByIndex(indexPath: indexPath))
        
        return teamCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.showDetailTeam(row: indexPath.row)
    }
}

//MARK: - completionTable View Data source
extension TeamListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.autocompleteListCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = self.presenter.configureAutoCompleteCell(indexPath: indexPath)
        return cell
    }
}

//MARK: - completionTable View Delegate
extension TeamListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.searchTeamBySuggestionListRow(row: indexPath.row)
    }
}

//MARK: - searchBar Delegate
extension TeamListView: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text else {return}
        presenter.searchSuggestionByQuerry(text: query)
    }
}

//MARK: - Collection Delegate FlowLayout
extension TeamListView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat = LayoutConstant.padding
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: LayoutConstant.spacing, left: LayoutConstant.spacing, bottom: LayoutConstant.spacing, right: LayoutConstant.spacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LayoutConstant.spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return LayoutConstant.spacing
    }
}

//MARK: - TeamListView Delegate
extension TeamListView: TeamListViewProtocol {
    
    func updateList(indexPath: IndexPath) {
        collectionView.reloadItems(at: [indexPath])
    }
    
    func showSuggestionTableView() {
        completionTableView.isHidden = false
    }
    
    func dissmissSuggestionTableView() {
        completionTableView.isHidden = true
    }
    
    func reloadList() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func reloadSuggestionList() {
        self.completionTableView.reloadData()
    }
    
    func showDetails(detailPresenter: TeamDetailPresenter) {
        let teamDetailViewController = TeamDetailView(detailpresenter: detailPresenter)
        self.navigationController?.pushViewController(teamDetailViewController, animated: false)
    }
    
    func displayLoading() {
        loading.startAnimating()
    }
    
    func dismissLoading() {
        loading.stopAnimating()
    }
    
    func showFailed(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("ERR0R", comment: ""), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
