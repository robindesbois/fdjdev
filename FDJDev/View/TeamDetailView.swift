//
//  TeamDetailViewController.swift
//  FDJDev
//
//  Created by Regis Alla on 17/09/2022.
//

import Foundation
import UIKit

private enum LayoutConstant {
    static let bannerHeight: CGFloat = 100.0
    static let spacing: CGFloat = 8
    static let padding: CGFloat = 8
}

protocol TeamDetailViewProtocol {
    func showDetailTitle(title: String)
    func showDetails(countryName: String, championship: String, description: String)
    func showBanner(urlString: String)
}

class TeamDetailView: UIViewController {
    var detailpresenter: TeamDetailPresenter
    
    init(detailpresenter: TeamDetailPresenter) {
        self.detailpresenter = detailpresenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var banner: UIImageView = {
        let banner = UIImageView()
        banner.contentMode = .scaleAspectFit
        return banner
    }()
    
    private lazy var contryLabel: UILabel = {
        let contryLabel = UILabel()
        return contryLabel
    }()
    
    private lazy var championshipLabel: UILabel = {
        let contryLabel = UILabel()
        contryLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        
        return contryLabel
    }()
    
    private lazy var descriptionText: UITextView = {
        let championshipLabel = UITextView()
        championshipLabel.translatesAutoresizingMaskIntoConstraints = false
        championshipLabel.font = UIFont.systemFont(ofSize: 17.0)
        championshipLabel.textContainerInset = UIEdgeInsets.zero
        championshipLabel.textContainer.lineFragmentPadding = 0
        return championshipLabel
    }()
    
    private lazy var descriptionContent: UIView = {
        let descriptionContent = UIView()
        descriptionContent.translatesAutoresizingMaskIntoConstraints = false
        descriptionContent.addSubview(descriptionText)
        return descriptionContent
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(frame: self.view.bounds)
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = LayoutConstant.spacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(self.banner)
        stackView.addArrangedSubview(self.contryLabel)
        stackView.addArrangedSubview(self.championshipLabel)
        stackView.addArrangedSubview(self.descriptionContent)
        
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailpresenter.setView(self)
        setupViews()
        setupLayouts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        detailpresenter.showDetails()
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        self.view.addSubview(stackView)
    }
    
    private func setupLayouts() {
        // Layout constraints for `stackView`
        self.stackView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: LayoutConstant.padding).isActive = true
        self.stackView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -LayoutConstant.padding).isActive = true
        self.stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: LayoutConstant.padding).isActive = true
        self.stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -LayoutConstant.padding).isActive = true
        
        // Layout constraints for `banner`
        self.banner.heightAnchor.constraint(equalToConstant: LayoutConstant.bannerHeight).isActive = true
        self.banner.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: LayoutConstant.padding).isActive = true
        self.banner.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -LayoutConstant.padding).isActive = true
        
        // Layout constraints for `descriptionText`
        self.descriptionText.topAnchor.constraint(equalTo: self.descriptionContent.topAnchor).isActive = true
        self.descriptionText.bottomAnchor.constraint(equalTo: self.descriptionContent.bottomAnchor).isActive = true
        self.descriptionText.leadingAnchor.constraint(equalTo: self.descriptionContent.leadingAnchor).isActive = true
        self.descriptionText.trailingAnchor.constraint(equalTo: self.descriptionContent.trailingAnchor).isActive = true
    }
}

//MARK: - TeamDetail View delegate
extension TeamDetailView: TeamDetailViewProtocol {
    func showDetailTitle(title: String) {
        self.title = title
    }
    
    func showDetails(countryName: String, championship: String, description: String) {
        self.contryLabel.text = countryName
        self.championshipLabel.text = championship
        self.descriptionText.text = description
    }
    
    func showBanner(urlString: String) {
        if let image = ImageCache.shared.cache.object(forKey: urlString as NSString) {
            self.banner.image = image
        }
    }
}
