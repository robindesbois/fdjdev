//
//  ListCompetitionPresenter.swift
//  FDJDev
//
//  Created by Regis Alla on 17/09/2022.
//

import Foundation

class TeamListViewPresenter {
    private let service : ApiServiceProtocol!
    private weak var listView : TeamListView?
    
    var errorMessage: String = "" {
        didSet {
            guard let view = self.listView else { return }
            view.showFailed(message: errorMessage)
        }
    }
    
    var leagueList: [String] = []
    var teamList: [Team] = [] {
        didSet {
            guard let view = self.listView else { return }
            view.reloadList()
        }
    }
    
    private var autocompleteList: [String] = [] {
        didSet {
            guard let view = self.listView else { return }
            view.reloadSuggestionList()
        }
    }
    
    init(service: ApiServiceProtocol = ApiService()) {
        self.service = service
    }
    
    func setView(view : TeamListView){
        self.listView = view
    }
    
    var teamListCount: Int {
        return teamList.count
    }
    
    var autocompleteListCount: Int {
        return self.autocompleteList.count
    }
    
    func configureAutoCompleteCell(indexPath: IndexPath) -> String{
        return autocompleteList[indexPath.row]
    }
    
    func downloadImageByIndex(indexPath: IndexPath) -> String? {
        let team = teamList[indexPath.row]
        if let imageURL = team.teamBadge {
            downloadImageWithPath(imageURL: imageURL) { (urlImage) in
                self.listView?.updateList(indexPath: indexPath)
            }
        }
        return team.teamBadge
    }
    
    func fetchAllLeagues() {
        service.getAllLeagues(completion: { (response) in
            switch response {
            case.done(value: let result):
                self.leagueList = result?.map({$0.strLeague!}) ?? []
                break
            case .failed(let message):
                self.errorMessage = message
                break
            }
        })
    }
    
    // Autocomplete function
    func searchSuggestionByQuerry(text: String) {
        guard let view = self.listView else { return }
        view.showSuggestionTableView()
        
        self.teamList = []
        self.autocompleteList = leagueList.filter({$0.contains(text)})
    }
    
    // Function that searches for teams based on league suggestion
    func searchTeamBySuggestionListRow(row: Int) {
        guard let view = self.listView else { return }
        view.dissmissSuggestionTableView()
        
        let querry = autocompleteList[row]
        self.searchTeamFromLeague(text: querry)
    }
    
    func searchTeamFromLeague(text: String) {
        if let view = self.listView {
            view.displayLoading()
        }
        service.searchTeamByQuerry(text: text, completion: { (response) in
            if let view = self.listView {
                view.dismissLoading()
            }
            switch response {
            case.done(value: let result):
                self.teamList = result ?? []
                break
            case .failed(let message):
                self.errorMessage = message
                break
            }
        })
    }
    
    private func downloadImageWithPath(imageURL: String, completionHandler: @escaping (Bool) -> ()) {
        service.downloadImageWithPath(imagePath: imageURL) { downloadFinish in
            completionHandler(downloadFinish)
        }
    }
    
    func showDetailTeam(row: Int) {
        guard let view = self.listView else { return }
        let detailPresenter = TeamDetailPresenter(team: teamList[row])
        view.showDetails(detailPresenter: detailPresenter)
    }
}
