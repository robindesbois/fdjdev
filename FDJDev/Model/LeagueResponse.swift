//
//  LeagueResponse.swift
//  FDJDev
//
//  Created by Regis Alla on 18/09/2022.
//

import Foundation

struct LeagueResponse: Codable {
    var leagues: [League]?
}
