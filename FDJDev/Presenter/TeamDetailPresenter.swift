//
//  TeamDetailPresenter.swift
//  FDJDev
//
//  Created by Regis Alla on 18/09/2022.
//

import Foundation

class TeamDetailPresenter {
    private let service: ApiService!
    
    private weak var view: TeamDetailView?
    private var team: Team!
    
    init(team: Team) {
        self.team = team
        self.service = ApiService()
    }
    
    func setView(_ view: TeamDetailView) {
        self.view = view
    }
    
    func showDetails() {
        self.showDetails(team: team)
        self.downloadImageWithPath(imageURL: team.teamBanner)
    }
    
    private func showDetails(team: Team) {
        guard let view = self.view else { return }
        if let title = team.name {
            view.showDetailTitle(title: title)
        }
        
        guard let country = team.country, let league = team.league, let description = team.description  else { return }
        view.showDetails(countryName: country, championship: league, description: description)
    }
    
    func downloadImageWithPath(imageURL: String?) {
        if let view = self.view {
            service.downloadImageWithPath(imagePath: imageURL ?? "") { downloadFinish in
                view.showBanner(urlString: imageURL ?? "")
            }
        }
    }
}
