//
//  TeamListViewPresenterTests.swift
//  FDJDevTests
//
//  Created by Regis Alla on 19/09/2022.
//

import XCTest
@testable import FDJDev

class TeamListViewPresenterTests: XCTestCase {    
    var presenter: TeamListViewPresenter!
    var mockSercice: MockApiService!
    
    override func setUp() {
        mockSercice = MockApiService()
        presenter = .init(service: mockSercice)
    }
    
    func testFetchingAllLeaguesOnSuccess() {
        let league1 = League(strLeague: "France")
        let league2 = League(strLeague: "France2")
        
        mockSercice.resultLeague = .done([league1, league2])
        presenter.fetchAllLeagues()
        
        XCTAssertNotNil(presenter.leagueList)
        XCTAssertEqual(presenter.leagueList.count, 2)
    }
    
    func testFetchingAllLeaguesWithError() {
        mockSercice.resultLeague = .failed(message: NSLocalizedString("ERR0R_SERVER", comment: ""))
        presenter.fetchAllLeagues()
        
        XCTAssertEqual(presenter.leagueList.count, 0)
        XCTAssertTrue(!presenter.errorMessage.isEmpty)
        XCTAssertEqual(presenter.errorMessage, NSLocalizedString("ERR0R_SERVER", comment: ""))
    }
    
    func testFetchingResultSearchTeamFromLeagueOnSuccess() {
        let team1 = Team(idTeam: "1111", country: "test1", league: "test1", description: "test1", teamBanner: "test1", name: "test1", teamBadge: "test1")
        let team2 = Team(idTeam: "2222", country: "test2", league: "test2", description: "test2", teamBanner: "test2", name: "test2", teamBadge: "test2")
        
        mockSercice.resultSearchTeam = .done([team1, team2])
        
        presenter.searchTeamFromLeague(text: "French Ligue 1")
        
        XCTAssertNotNil(presenter.teamList)
        XCTAssertTrue(!presenter.teamList.isEmpty)
        XCTAssertEqual(presenter.teamList.count, 2)
    }
    
    func testFetchingResultSearchTeamFromLeagueWhitError() {
        mockSercice.resultSearchTeam = .done(nil)
        presenter.searchTeamFromLeague(text: "French111111")
        
        XCTAssertNotNil(presenter.teamList)
        XCTAssertTrue(presenter.teamList.isEmpty)
        XCTAssertTrue(presenter.errorMessage.isEmpty)
    }
}
