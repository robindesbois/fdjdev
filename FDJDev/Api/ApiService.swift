//
//  CompetitionService.swift
//  FDJDev
//
//  Created by Regis Alla on 17/09/2022.
//

import Foundation
import Alamofire

protocol ApiServiceProtocol {
    func getAllLeagues(completion: @escaping (_ result: RequestResult<[League]?>) -> Void)
    
    func searchTeamByQuerry(text: String, completion: @escaping (_ result: RequestResult<[Team]?>) -> Void)
    
    func downloadImageWithPath(imagePath: String, completionHandler: @escaping (Bool) -> ())
}

class ApiService: ApiServiceProtocol {
    
    static let APIKEY = "50130162"
    static let BASE_URL : String = "https://www.thesportsdb.com/api/v1/json/"
    static let URL_ALL_LEAGUE = "https://www.thesportsdb.com/api/v1/json/2/all_leagues.php"
    var searchAllTeamsUrl : String = "\(BASE_URL)\(APIKEY)/search_all_teams.php"
    
    
    
    ///Function that allows you to retrieve the list of leagues
    ///
    /// - returns: [League] representation of list of leagues
    
    func getAllLeagues(completion: @escaping (_ result: RequestResult<[League]?>) -> Void) {
        
        Alamofire.request(ApiService.URL_ALL_LEAGUE, method: .get, parameters: nil, encoding: URLEncoding.default)
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    do {
                        let responseContent = try JSONDecoder().decode(LeagueResponse.self, from: response.data!)
                        completion(RequestResult.done(responseContent.leagues))
                    } catch {
                        completion(RequestResult.failed(message: ErrorHandler.ErrorType.parsingError.errorDescription))
                    }
                case .failure(let error):
                    completion(RequestResult.failed(message: ErrorHandler.getErrorType(forServerError: error).errorDescription))
                    print("")
                }
            }
    }
    
    /// Function that allows you to perform a search based on a query
    ///
    /// - parameter text: text to search
    /// - returns: [Team]  representation of state team list
    func searchTeamByQuerry(text: String, completion: @escaping (_ result: RequestResult<[Team]?>) -> Void) {
        let parameters: Parameters = ["l": text]
        
        Alamofire.request(searchAllTeamsUrl, method: .post, parameters: parameters, encoding: URLEncoding.queryString)
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    do {
                        let responseContent = try JSONDecoder().decode(TeamResponse.self, from: response.data!)
                        completion(RequestResult.done(responseContent.teams))
                    } catch {
                        completion(RequestResult.failed(message: ErrorHandler.ErrorType.parsingError.errorDescription))
                    }
                case .failure(let error):
                    completion(RequestResult.failed(message: ErrorHandler.getErrorType(forServerError: error).errorDescription))
                    print("")
                }
            }
    }
    
    /// Function that allows to download an image from a path and store image in cache
    ///
    /// - parameter imagePath: path of image
    /// - returns: Bool representation of download status
    func downloadImageWithPath(imagePath: String, completionHandler: @escaping (Bool) -> ()) {
        let placeholder = UIImage(named: "ic_no_image")
        
        if ImageCache.shared.cache.object(forKey: imagePath as NSString) != nil {
            completionHandler(true)
        } else {
            Alamofire.request(imagePath, method: .get, parameters: nil, encoding: URLEncoding(destination: .queryString),headers: nil)
                .responseData { response in
                    switch response.result {
                    case .success:
                        DispatchQueue.main.async {
                            if let image = UIImage(data: response.data!) {
                                ImageCache.shared.cache.setObject(image, forKey: imagePath as NSString)
                            } else {
                                ImageCache.shared.cache.setObject(placeholder!, forKey: imagePath as NSString)
                            }
                            completionHandler(true)
                        }
                    case .failure(_):
                        ImageCache.shared.cache.setObject(placeholder!, forKey: imagePath as NSString)
                        completionHandler(false)
                    }
                }
        }
    }
}
